#Firmware
PRODUCT_COPY_FILE += \
	vendor/xiaomi/firmware/sirius/abl.elf:install/firmware-update/abl.elf \
	vendor/xiaomi/firmware/sirius/aop.mbn:install/firmware-update/aop.mbn \
	vendor/xiaomi/firmware/sirius/BTFM.bin:install/firmware-update/BTFM.bin \
	vendor/xiaomi/firmware/sirius/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
	vendor/xiaomi/firmware/sirius/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
	vendor/xiaomi/firmware/sirius/devcfg.mbn:install/firmware-update/devcfg.mbn \
	vendor/xiaomi/firmware/sirius/dspso.bin:install/firmware-update/dspso.bin \
	vendor/xiaomi/firmware/sirius/dtbo.img:install/firmware-update/dtbo.img \
	vendor/xiaomi/firmware/sirius/hyp.mbn:install/firmware-update/hyp.mbn \
	vendor/xiaomi/firmware/sirius/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
	vendor/xiaomi/firmware/sirius/logfs_ufs_8mb.bin:install/firmware-update/logfs_ufs_8mb.bin \
	vendor/xiaomi/firmware/sirius/logo.img:install/firmware-update/logo.img \
	vendor/xiaomi/firmware/sirius/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
	vendor/xiaomi/firmware/sirius/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
	vendor/xiaomi/firmware/sirius/storsec.mbn:install/firmware-update/storsec.mbn \
	vendor/xiaomi/firmware/sirius/tz.mbn:install/firmware-update/tz.mbn \
	vendor/xiaomi/firmware/sirius/xbl.elf:install/firmware-update/xbl.elf \
	vendor/xiaomi/firmware/sirius/xbl_config.elf:install/firmware-update/xbl_config.elf 